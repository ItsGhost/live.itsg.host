import * as matrix from "matrix-js-sdk";
import type {AxiosResponse} from "axios";
import axios from "axios"
import {defineStore} from "pinia";

export interface State {
    matrixClient: matrix.MatrixClient
    streamTitle: string
    streamLive: boolean
}

export const useStore = defineStore('main', {
    state: (): State => {
        return {
            matrixClient: matrix.createClient({
                baseUrl: "https://matrix.itsg.host"
            }),
            streamTitle: 'Ghost Live',
            streamLive: false,
        }
    },

    actions: {
        watchTitle(stream: string): void {
            const roomName = `#comments_live.itsg.host_${stream}:itsg.host`;

            this.matrixClient.getRoomIdForAlias(roomName).then((room: any) => {
                this.matrixClient.publicRooms({}).then((result: any) => {
                    const title = result.chunk.find((r: matrix.IPublicRoomsChunkRoom) => {
                        return r.room_id == room.room_id
                    })?.name;
                    if (title) {
                        this.streamTitle = title;
                    }
                });
            }).finally(() => {
                setTimeout(() => {
                    this.watchTitle(stream);
                }, 15000);
            })
        },

        watchLive(stream: string): void {
            axios.get(`https://${import.meta.env.VITE_APP_OVEN_APP as string}/${stream}/abr.m3u8`).then(() => {
                this.streamLive = true
            }).catch((_: AxiosResponse) => {
                this.streamLive = false
            }).finally(() => {
                setTimeout(() => {
                    this.watchLive(stream)
                }, 5000);
            })
        }
    }
})

