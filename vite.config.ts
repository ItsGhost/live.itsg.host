import {fileURLToPath, URL} from 'url'

import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
// import vueJsx from '@vitejs/plugin-vue-jsx'
import { NodeGlobalsPolyfillPlugin } from '@esbuild-plugins/node-globals-polyfill'
import { NodeModulesPolyfillPlugin } from '@esbuild-plugins/node-modules-polyfill'
import nodePolyfills from 'rollup-plugin-node-polyfills';
import elmPlugin from 'vite-plugin-elm'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue({
        }),
        // vueJsx()
        elmPlugin(),
    ],
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    },
    optimizeDeps: {
        esbuildOptions: {
            // Node.js global to browser globalThis
            define: {
                global: 'globalThis'
            },
            // Enable esbuild polyfill plugins
            plugins: [
                NodeGlobalsPolyfillPlugin({
                    process: true,
                    buffer: true
                }),
                NodeModulesPolyfillPlugin()
            ]
        }
    },
    build: {
        assetsInlineLimit: 40000,
        rollupOptions: {
            plugins: [
                // Enable rollup polyfills plugin
                // used during production bundling
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                //@ts-ignore
                nodePolyfills()
            ]
        }
    },
})
